"""
Ingame Commands

These type of commands allow players and NPCs to interact
the world.

Classes:
    CmdBuy
    CmdSell
    CmdEquip
    CmdCoins
    CmdPut
    CmdFromGet

"""

from django.conf import settings
from evennia.commands.default import general
from evennia.utils import utils
from world.dsa1.ruleset import in_silbertaler

# error return function, needed by Extended Look command
_AT_SEARCH_RESULT = utils.variable_from_module(*settings.SEARCH_AT_RESULT.rsplit(".", 1))

COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)


class CmdBuy(COMMAND_DEFAULT_CLASS):
    """
    buy an item

    Usage:
      buy <obj>

    Buy an item in a shop.
    """
    key = "buy"
    help_category = "Ingame"
    locks = "cmd:all()"
    
    def func(self):
        """
        Handle buying an item.
        """
        
        caller = self.caller

        if not self.args:
            caller.msg("Buy what?")
            return
        obj = caller.search(self.args, location=caller.location)
        if not obj:
            return
        if caller == obj:
            caller.msg("You can't buy yourself.")
            return
        if not obj.access(caller, "buy"):
            if obj.db.buy_err_msg:
                caller.msg(obj.db.buy_err_msg)
            else:
                caller.msg("You can't buy that.")
            return
        
        # obj.move_to(caller, quiet=True)
        
        if in_silbertaler(caller.db.coins) >= obj.db.price:
            caller.msg("You can buy it.")
            
        else:
            caller.msg("You don't have enough money for that.")


class CmdSell(COMMAND_DEFAULT_CLASS):
    """
    sell an item

    Usage:
      sell <obj>

    Sell an item from your inventory to a shop.
    """
    key = "sell"
    help_category = "Ingame"
    locks = "cmd:all()"
    
    def func(self):
        """
        Handle selling an item.
        """
        
        self.msg('selling item')


class CmdEquip(COMMAND_DEFAULT_CLASS):
    """
    equip an item
    
    Usage:
      equip <obj>
    
    Equip an item like a weapon or armor.
    """
    
    key = "equip"
    aliases = ["eq", "unequip", "uneq"]
    help_category = "Ingame"
    
    def func(self):
        """implements the command."""
        
        caller = self.caller
        
        if self.cmdname not in ["unequip","uneq"]:
            """equip"""
            
            if not self.args:
                caller.msg("Equip what?")
                return
            
            obj = caller.search(
                self.args,
                location=caller,
                nofound_string="You aren't carrying %s." % self.args,
                multimatch_string="You carry more than one %s:" % self.args,
            )
            if not obj:
                return
            
            # equip acording to if it's a weapon or an armor
            if obj.db.damage is not None:
                caller.db.weapon = obj
                caller.msg(f"You start to wield the {obj.name}.")
            elif obj.db.armor is not None:
                caller.db.armor = obj
                caller.msg(f"You start to wear the {obj.name}.")

        else:
            """unequip"""
            objs = caller.search(
                self.args,
                candidates=[caller.db.weapon, caller.db.armor],
                quiet = True,
            )
            if len(objs) > 0:
                obj = objs[0]
                caller.msg(f"unequiping {obj.name}")
                if caller.db.weapon is obj:
                    caller.db.weapon = None
                elif caller.db.armor is obj:
                    caller.db.armor = None
            else:
                if self.args in ["weapon", "w"] and caller.db.weapon is not None:
                    caller.msg(f"unequiping {caller.db.weapon.name}")
                    caller.db.weapon = None
                elif self.args in ["armor", "a"] and caller.db.armor is not None:
                    caller.msg(f"unequiping {caller.db.armor.name}")
                    caller.db.armor = None
                else:
                    caller.msg("Unequip what?")
                    return


class CmdCoins(general.CmdInventory):

    key = "coins"
    aliases = ["currency", "c"]
    help_category = "Ingame"
    
    def func(self):
        """check currency"""
        
        colors = {
            "dukaten": "|y",
            "silbertaler": "|w",
            "heller": "|Y",
            "kreuzer": "|R",
        }
        
        currency_list = []
        for key in self.caller.db.coins:
            if self.caller.db.coins[key] > 0:
                if key == "dukaten" and self.caller.db.coins[key] == 1:
                    # dukaten singular
                    currency_list.append(f"{colors[key]}{self.caller.db.coins[key]}|n Dukate")
                else:
                    try:
                        # with color tag
                        currency_list.append(f"{colors[key]}{self.caller.db.coins[key]}|n {key.capitalize()}")
                    except Exception:
                        # without color tag
                        currency_list.append(f"{self.caller.db.coins[key]}|n {key}")
        self.msg("|wCoins:|n|/{}".format(", ".join(currency_list)))


class CmdPut(COMMAND_DEFAULT_CLASS):
    """
    Puts an object inside a container
    Usage:
        put <object> in <object>
    Places an object you hold inside an unlocked
    container.
    """

    key = "lege"
    aliases = ["put"]
    help_category = "Ingame"
    locks = "cmd:all()"

    def func(self):
        """Executes Put command"""

        caller = self.caller

        args = self.args.split(" in ", 1)
        if len(args) != 2:
            caller.msg("Usage: put <name> in <name>")
            return
        dest = caller.search(args[1], use_nicks=True, quiet=True)
        if not dest:
            return _AT_SEARCH_RESULT(dest, caller, args[1])
        dest = utils.make_iter(dest)[0]

        obj = caller.search(args[0], location=caller)
        if not obj:
            return
        obj_list = [obj]

        obj_list = [ob for ob in obj_list if ob.at_before_move(dest, caller=caller)]
        success = []
        for obj in obj_list:
            if obj == dest:
                caller.msg("You can't put an object inside itself.")
                continue
            if not dest.typename == "ContainerObject":
                caller.msg("That is not a container.")
                return
            if dest.db.is_locked and not self.caller.check_permstring(
                "builders"
            ):
                caller.msg("You'll have to unlock {} first.".format(dest.name))
                return
            if dest in obj.contents:
                caller.msg("You can't place an object in something it contains.")
                continue

            if not obj.access(caller, "get"):
                caller.msg("You cannot move {}.".format(obj))
                continue
            obj.move_to(dest)
            success.append(obj)

        if success:
            success_str = "%s in %s" % (utils.list_to_string(success), dest.name)
            caller.msg("You put %s." % success_str)
            caller.location.msg_contents(
                "%s puts %s." % (caller.name, success_str), exclude=caller
            )
        else:
            self.msg("Nothing moved.")


class CmdFromGet(COMMAND_DEFAULT_CLASS):
    """
    take an item out of a container
    
    Usage:
      aus <container> nimm <obj>
    
    Example: aus tasche nimm apfel.
    """
    key = "aus"
    help_category = "Ingame"
    locks = "cmd:all()"
    
    def func(self):
        
        caller = self.caller
        
        args = self.args
        args = args.split(" nimm ", 1)
        container_str = args[0]
        obj_str = args[1]
        
        container = caller.search(
            container_str,
            location=[caller, caller.location],
            nofound_string="You can't find %s." % container_str,
            multimatch_string="There is more than one %s:" % container_str,
        )
        if not container:
            return
        
        obj = caller.search(
            obj_str,
            location=container,
            nofound_string="You can't find %s in %s." % (self.args, container.key),
            multimatch_string="There is more than one %s in %s:" % (self.args, container.key),
        )
        if not container:
            return
        
        """ all the following is similar to CmdGet """
        
        if caller == obj:
            caller.msg("You can't get yourself.")
            return
        if not obj.access(caller, "get"):
            if obj.db.get_err_msg:
                caller.msg(obj.db.get_err_msg)
            else:
                caller.msg("Du kannst das nicht nehmen.")
            if obj.db.get_err_msg_others:
                caller.location.msg_contents(caller.name + " " + obj.db.get_err_msg_others, exclude=caller)
            return

        # calling at_before_get hook method
        if not obj.at_before_get(caller):
            return

        success = obj.move_to(caller, quiet=True)
        if not success:
            caller.msg("This can't be picked up.")
        else:
            caller.msg("You pick up %s." % obj.name)
            caller.location.msg_contents(
                "%s picks up %s." % (caller.name, obj.name), exclude=caller
            )
            # calling at_get hook method
            obj.at_get(caller)

