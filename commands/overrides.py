"""
Overridden Commands

These are a couple of the core commands of Evennia.
But with some changes like: translations or added features.

Classes:
    CmdCharCreate
    CmdCreate
    CmdDrop
    CmdGet
    CmdGive
    CmdHelp
    CmdOOC
    CmdPose
    CmdSay
    CmdSpawn
    CmdTunnel
    CmdWhisper

"""

from collections import defaultdict

from django.conf import settings
from evennia import default_cmds
from evennia.commands.default import account, building, general, help
from evennia.help.models import HelpEntry
from evennia.prototypes import spawner, prototypes as protlib, menus as olc_menus
from evennia.utils import create, utils
from evennia.utils.evmenu import EvMenu
from evennia.utils.utils import string_suggestions, list_to_string

COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)
CMD_IGNORE_PREFIXES = settings.CMD_IGNORE_PREFIXES


class CmdCharCreate(account.CmdCharCreate):
    """
    create a new character

    Usage:
      charcreate

    Create a new character. You may use upper-case letters in the
    name - you will nevertheless always be able to access your
    character using lower-case letters if you want.
    """

    def func(self):
        """create the new character"""
        """customized: run a chargen menu"""
        
        if self.caller.db_typeclass_path == "typeclasses.accounts.Account":
            EvMenu(self.caller, "world.dsa1.chargen_menu", session=self.session)
        else:
            self.msg("Please log out with this Character first (Command: ooc)")


class CmdCreate(default_cmds.ObjManipCommand):
    """
    create new objects

    Usage:
      create[/drop] <objname>[;alias;alias...][:typeclass], <objname>...

    switch:
       drop - automatically drop the new object into your current
              location (this is not echoed). This also sets the new
              object's home to the current location rather than to you.

    Creates one or more new objects. If typeclass is given, the object
    is created as a child of this typeclass. The typeclass script is
    assumed to be located under types/ and any further
    directory structure is given in Python notation. So if you have a
    correct typeclass 'RedButton' defined in
    types/examples/red_button.py, you could create a new
    object of this type like this:

       create/drop button;red : examples.red_button.RedButton

    """

    key = "create"
    switch_options = ("drop",)
    locks = "cmd:perm(create) or perm(Builder)"
    help_category = "Building"

    # lockstring of newly created objects, for easy overloading.
    # Will be formatted with the {id} of the creating object.
    new_obj_lockstring = "control:id({id}) or perm(Admin);delete:id({id}) or perm(Admin)"

    def func(self):
        """
        Creates the object.
        """
        """
        customized: create multiple objects with descriptions
        seperate the descriptions with a comma
        """

        caller = self.caller

        if not self.args:
            string = "Usage: create[/drop] <newname>[;alias;alias...] [:typeclass.path]"
            caller.msg(string)
            return
        
        # create the objects
        for i, objdef in enumerate(self.lhs_objs):
            string = ""
            name = objdef["name"]
            aliases = objdef["aliases"]
            typeclass = objdef["option"]
            
            # description
            try:
                attributes = [("desc", self.rhs.split(",")[i])]
            except (IndexError, AttributeError):
                attributes = None

            # create object (if not a valid typeclass, the default
            # object typeclass will automatically be used)
            lockstring = self.new_obj_lockstring.format(id=caller.id)
            obj = create.create_object(
                typeclass,
                name,
                caller,
                home=caller,
                aliases=aliases,
                locks=lockstring,
                report_to=caller,
                attributes=attributes,
            )
            if not obj:
                continue
            if aliases:
                string = "You create a new %s: %s (aliases: %s)."
                string = string % (obj.typename, obj.name, ", ".join(aliases))
            else:
                string = "You create a new %s: %s."
                string = string % (obj.typename, obj.name)
            # set a default desc
            if not obj.db.desc:
                obj.db.desc = "You see nothing special."
            if "drop" in self.switches:
                if caller.location:
                    obj.home = caller.location
                    obj.move_to(caller.location, quiet=True)
        if string:
            caller.msg(string)


class CmdDrop(general.CmdDrop):
    key = "fallen lassen"
    aliases = ["fl", "drop"]
    help_category = "Ingame"


class CmdGet(general.CmdGet):
    """
    pick up something

    Usage:
      get <obj>

    Picks up an object from your location and puts it in
    your inventory.
    """

    key = "nimm"
    aliases = ["get", "grab"]
    locks = "cmd:all()"
    help_category = "Ingame"
    arg_regex = r"\s|$"

    def func(self):
        """implements the command."""

        caller = self.caller

        if not self.args:
            caller.msg("Nimm was?")
            return
        obj = caller.search(self.args, location=caller.location)
        if not obj:
            return
        if caller == obj:
            caller.msg("You can't get yourself.")
            return
        if not obj.access(caller, "get"):
            if obj.db.get_err_msg:
                caller.msg(obj.db.get_err_msg)
            else:
                caller.msg("Du kannst das nicht nehmen.")
            if obj.db.get_err_msg_others:
                caller.location.msg_contents(caller.name + " " + obj.db.get_err_msg_others, exclude=caller)
            return

        # calling at_before_get hook method
        if not obj.at_before_get(caller):
            return

        success = obj.move_to(caller, quiet=True)
        if not success:
            caller.msg("This can't be picked up.")
        else:
            caller.msg("You pick up %s." % obj.name)
            caller.location.msg_contents(
                "%s picks up %s." % (caller.name, obj.name), exclude=caller
            )
            # calling at_get hook method
            obj.at_get(caller)


class CmdGive(general.CmdGive):
    key = "gib"
    aliases = ["gebe", "give"]
    help_category = "Ingame"


class CmdHelp(help.CmdHelp):
    def func(self):
        """
        Run the dynamic help entry creator.
        
        customized: show command permissions
        
        """
        query, cmdset = self.args, self.cmdset
        caller = self.caller

        suggestion_cutoff = self.suggestion_cutoff
        suggestion_maxnum = self.suggestion_maxnum

        if not query:
            query = "all"

        # removing doublets in cmdset, caused by cmdhandler
        # having to allow doublet commands to manage exits etc.
        cmdset.make_unique(caller)

        # retrieve all available commands and database topics
        all_cmds = [cmd for cmd in cmdset if self.check_show_help(cmd, caller)]

        # generate command chars based on permissions
        cmd_chars = {}
        for i, mycmd in enumerate(all_cmds):
            for lock in mycmd.locks.split(";"):
                if lock.startswith("cmd:"):
                    if "perm(Builder)" in lock:
                        cmd_chars[mycmd.key] = "@"
                    elif "perm(Admin)" in lock:
                        cmd_chars[mycmd.key] = "+"
                    elif "perm(Developer)" in lock:
                        cmd_chars[mycmd.key] = "#"
                    elif "superuser()" in lock:
                        cmd_chars[mycmd.key] = "~"
        
        all_topics = [
            topic for topic in HelpEntry.objects.all() if topic.access(caller, "view", default=True)
        ]
        all_categories = list(
            set(
                [cmd.help_category.lower() for cmd in all_cmds]
                + [topic.help_category.lower() for topic in all_topics]
            )
        )

        if query in ("list", "all"):
            # we want to list all available help entries, grouped by category
            hdict_cmd = defaultdict(list)
            hdict_topic = defaultdict(list)
            # create the dictionaries {category:[topic, topic ...]} required by format_help_list
            # Filter commands that should be reached by the help
            # system, but not be displayed in the table, or be displayed differently.
            for cmd in all_cmds:
                if self.should_list_cmd(cmd, caller):
                    key = (
                        cmd.auto_help_display_key
                        if hasattr(cmd, "auto_help_display_key")
                        else cmd.key
                    )
                    
                    # add command char
                    # if it's not the first character of the command already
                    if key in cmd_chars and key[0] != cmd_chars[key]:
                        key = cmd_chars[key] + key
                    
                    hdict_cmd[cmd.help_category].append(key)
            [hdict_topic[topic.help_category].append(topic.key) for topic in all_topics]
            # report back
            self.msg_help(self.format_help_list(hdict_cmd, hdict_topic))
            return

        # Try to access a particular command

        # build vocabulary of suggestions and rate them by string similarity.
        suggestions = None
        if suggestion_maxnum > 0:
            vocabulary = (
                [cmd.key for cmd in all_cmds if cmd]
                + [topic.key for topic in all_topics]
                + all_categories
            )
            [vocabulary.extend(cmd.aliases) for cmd in all_cmds]
            suggestions = [
                sugg
                for sugg in string_suggestions(
                    query, set(vocabulary), cutoff=suggestion_cutoff, maxnum=suggestion_maxnum
                )
                if sugg != query
            ]
            if not suggestions:
                suggestions = [
                    sugg for sugg in vocabulary if sugg != query and sugg.startswith(query)
                ]

        # try an exact command auto-help match
        match = [cmd for cmd in all_cmds if cmd == query]

        if not match:
            # try an inexact match with prefixes stripped from query and cmds
            _query = query[1:] if query[0] in CMD_IGNORE_PREFIXES else query

            match = [
                cmd
                for cmd in all_cmds
                for m in cmd._matchset
                if m == _query or m[0] in CMD_IGNORE_PREFIXES and m[1:] == _query
            ]

        if len(match) == 1:
            cmd = match[0]
            key = cmd.auto_help_display_key if hasattr(cmd, "auto_help_display_key") else cmd.key
            formatted = self.format_help_entry(
                key, cmd.get_help(caller, cmdset), aliases=cmd.aliases, suggested=suggestions,
            )
            self.msg_help(formatted)
            return

        # try an exact database help entry match
        match = list(HelpEntry.objects.find_topicmatch(query, exact=True))
        if len(match) == 1:
            formatted = self.format_help_entry(
                match[0].key,
                match[0].entrytext,
                aliases=match[0].aliases.all(),
                suggested=suggestions,
            )
            self.msg_help(formatted)
            return

        # try to see if a category name was entered
        if query in all_categories:
            self.msg_help(
                self.format_help_list(
                    {
                        query: [
                            cmd.auto_help_display_key
                            if hasattr(cmd, "auto_help_display_key")
                            else cmd.key
                            for cmd in all_cmds
                            if cmd.help_category == query
                        ]
                    },
                    {query: [topic.key for topic in all_topics if topic.help_category == query]},
                )
            )
            return

        # no exact matches found. Just give suggestions.
        self.msg(
            self.format_help_entry(
                "", f"No help entry found for '{query}'", None, suggested=suggestions
            ),
            options={"type": "help"},
        )


class CmdInventory(general.CmdInventory):
    help_category = "Ingame"
    
    def func(self):
        """check inventory"""
        items = self.caller.contents
        if not items:
            string = "You are not carrying anything."
        else:
            from evennia.utils.ansi import raw as raw_ansi

            table = self.styled_table(border="header")
            for item in items:
                # name
                row = (f"|C{item.name}|n",)
                
                # item attribute
                if item.db.damage is not None:
                    if item.db.damage[0] == 1:
                        row += (f"w+{item.db.damage[1]}",)
                    else:
                        row += (f"{item.db.damage[0]}w+{item.db.damage[1]}",)
                elif item.db.armor is not None:
                    row += (f"AR:{item.db.armor}",)
                else:
                    row += ("",)
                
                # equiped status
                if self.caller.db.weapon == item or self.caller.db.armor == item:
                    row += ("(w)",)
                else:
                    row += ("",)
                
                #self.msg(item.typeclass_path)
                #self.msg(item.typename)
                
                # description
                if item.db.desc is not None:
                    row += ("{}|n".format(utils.crop(raw_ansi(item.db.desc), width=45) or ""),)
                else:
                    row += ("{}|n".format(utils.crop(raw_ansi(""), width=45) or ""),)
                
                table.add_row(*row)
            string = f"|wYou are carrying:\n{table}"
        self.caller.msg(string)


# note that this is inheriting from MuxAccountLookCommand,
# and as such has the .playable property.
class CmdOOC(account.MuxAccountLookCommand):
    """
    stop puppeting and go ooc

    Usage:
      ooc

    Go out-of-character (OOC).

    This will leave your current character and put you in a incorporeal OOC state.
    """
    """customized: don't remove (location of) NPC characters after puppeteering"""

    key = "ooc"
    locks = "cmd:pperm(Player)"
    aliases = [";", "unpuppet"]
    help_category = "General"

    # this is used by the parent
    account_caller = True

    def func(self):
        """Implement function"""

        account = self.account
        session = self.session

        character = account.get_puppet(session)

        if not self.args:

            old_char = account.get_puppet(session)
            if not old_char:
                string = "You are already OOC."
                self.msg(string)
                return

            account.db._last_puppet = old_char

            # disconnect
            try:
                account.unpuppet_object(session)
                self.msg("\n|GYou go OOC.|n\n")

                if settings.MULTISESSION_MODE < 2:
                    # only one character allowed
                    self.msg("You are out-of-character (OOC).\nUse |wic|n to get back into the game.")
                    return

                self.msg(account.at_look(target=self.playable, session=session))

            except RuntimeError as exc:
                self.msg("|rCould not unpuppet from |c%s|n: %s" % (old_char, exc))

        else:
            if character is None:
                self.msg("|=kOOC %s: %s|n" % (account.key, self.args))
            else:
                msg = "|=kOOC %s(%s): %s|n" % (account.key, character.key, self.args)
                character.location.msg_contents(text=(msg, {"type": "ooc"}), from_obj=character)


class CmdPose(general.CmdPose):
    help_category = "Ingame"


class CmdSay(COMMAND_DEFAULT_CLASS):
    """
    speak as your character

    Usage:
      say <message>

    Talk to those in your current location.
    """

    key = "sag"
    aliases = ['"', "'", "sage", "say"]
    help_category = "Ingame"
    locks = "cmd:all()"

    def func(self):
        """Run the say command"""

        caller = self.caller

        if not self.args:
            caller.msg("Was sagen?")
            return

        speech = self.args

        # Calling the at_before_say hook on the character
        speech = caller.at_before_say(speech)

        # If speech is empty, stop here
        if not speech:
            return

        # Call the at_after_say hook on the character
        caller.at_say(speech, msg_self=True)


class CmdSpawn(building.CmdSpawn):
    def func(self):
        """Implements the spawner"""
        """customized: make it find lower, but also uppercase variable names"""

        caller = self.caller
        noloc = "noloc" in self.switches
        
        # make arguments lower case for matching
        self.args = self.args.lower() if self.args else None
        self.lhs = self.lhs.lower() if self.lhs else None
        self.rhs = self.rhs.lower() if self.rhs else None

        # run the menu/olc
        if (
            self.cmdstring == "olc"
            or "menu" in self.switches
            or "olc" in self.switches
            or "edit" in self.switches
        ):
            # OLC menu mode
            prototype = None
            if self.lhs:
                prototype_key = self.lhs
                prototype = self._search_prototype(prototype_key)
                if not prototype:
                    return
            olc_menus.start_olc(caller, session=self.session, prototype=prototype)
            return

        if "search" in self.switches:
            # query for a key match. The arg is a search query or nothing.

            if not self.args:
                # an empty search returns the full list
                self._list_prototypes()
                return

            # search for key;tag combinations
            key, _, tags = self._parse_key_desc_tags(self.args, desc=False)
            self._list_prototypes(key, tags)
            return

        if "raw" in self.switches:
            # query for key match and return the prototype as a safe one-liner string.
            if not self.args:
                caller.msg("You need to specify a prototype-key to get the raw data for.")
            prototype = self._search_prototype(self.args)
            if not prototype:
                return
            caller.msg(str(prototype))
            return

        if "show" in self.switches or "examine" in self.switches:
            # show a specific prot detail. The argument is a search query or empty.
            if not self.args:
                # we don't show the list of all details, that's too spammy.
                caller.msg("You need to specify a prototype-key to show.")
                return

            detail_string = self._get_prototype_detail(self.args)
            if not detail_string:
                return
            caller.msg(detail_string)
            return

        if "list" in self.switches:
            # for list, all optional arguments are tags.
            tags = self.lhslist
            err = self._list_prototypes(tags=tags)
            if err:
                caller.msg(
                    "No prototypes found with prototype-tag(s): {}".format(
                        list_to_string(tags, "or")
                    )
                )
            return

        if "save" in self.switches:
            # store a prototype to the database store
            if not self.args:
                caller.msg(
                    "Usage: spawn/save [<key>[;desc[;tag,tag[,...][;lockstring]]]] = <prototype_dict>"
                )
                return
            if self.rhs:
                # input on the form key = prototype
                prototype_key, prototype_desc, prototype_tags = self._parse_key_desc_tags(self.lhs)
                prototype_key = None if not prototype_key else prototype_key
                prototype_desc = None if not prototype_desc else prototype_desc
                prototype_tags = None if not prototype_tags else prototype_tags
                prototype_input = self.rhs.strip()
            else:
                prototype_key = prototype_desc = None
                prototype_tags = None
                prototype_input = self.lhs.strip()

            # handle parsing
            prototype = self._parse_prototype(prototype_input)
            if not prototype:
                return

            prot_prototype_key = prototype.get("prototype_key")

            if not (prototype_key or prot_prototype_key):
                caller.msg(
                    "A prototype_key must be given, either as `prototype_key = <prototype>` "
                    "or as a key 'prototype_key' inside the prototype structure."
                )
                return

            if prototype_key is None:
                prototype_key = prot_prototype_key

            if prot_prototype_key != prototype_key:
                caller.msg("(Replacing `prototype_key` in prototype with given key.)")
                prototype["prototype_key"] = prototype_key

            if prototype_desc is not None and prot_prototype_key != prototype_desc:
                caller.msg("(Replacing `prototype_desc` in prototype with given desc.)")
                prototype["prototype_desc"] = prototype_desc
            if prototype_tags is not None and prototype.get("prototype_tags") != prototype_tags:
                caller.msg("(Replacing `prototype_tags` in prototype with given tag(s))")
                prototype["prototype_tags"] = prototype_tags

            string = ""
            # check for existing prototype (exact match)
            old_prototype = self._search_prototype(prototype_key, quiet=True)

            diff = spawner.prototype_diff(old_prototype, prototype, homogenize=True)
            diffstr = spawner.format_diff(diff)
            new_prototype_detail = self._get_prototype_detail(prototypes=[prototype])

            if old_prototype:
                if not diffstr:
                    string = f"|yAlready existing Prototype:|n\n{new_prototype_detail}\n"
                    question = (
                        "\nThere seems to be no changes. Do you still want to (re)save? [Y]/N"
                    )
                else:
                    string = (
                        f'|yExisting prototype "{prototype_key}" found. Change:|n\n{diffstr}\n'
                        f"|yNew changed prototype:|n\n{new_prototype_detail}"
                    )
                    question = (
                        "\n|yDo you want to apply the change to the existing prototype?|n [Y]/N"
                    )
            else:
                string = f"|yCreating new prototype:|n\n{new_prototype_detail}"
                question = "\nDo you want to continue saving? [Y]/N"

            answer = yield (string + question)
            if answer.lower() in ["n", "no"]:
                caller.msg("|rSave cancelled.|n")
                return

            # all seems ok. Try to save.
            try:
                prot = protlib.save_prototype(prototype)
                if not prot:
                    caller.msg("|rError saving:|R {}.|n".format(prototype_key))
                    return
            except protlib.PermissionError as err:
                caller.msg("|rError saving:|R {}|n".format(err))
                return
            caller.msg("|gSaved prototype:|n {}".format(prototype_key))

            # check if we want to update existing objects

            self._update_existing_objects(self.caller, prototype_key, quiet=True)
            return

        if not self.args:
            # all switches beyond this point gets a common non-arg return
            ncount = len(protlib.search_prototype())
            caller.msg(
                "Usage: spawn <prototype-key> or {{key: value, ...}}"
                f"\n ({ncount} existing prototypes. Use /list to inspect)"
            )
            return

        if "delete" in self.switches:
            # remove db-based prototype
            prototype_detail = self._get_prototype_detail(self.args)
            if not prototype_detail:
                return

            string = f"|rDeleting prototype:|n\n{prototype_detail}"
            question = "\nDo you want to continue deleting? [Y]/N"
            answer = yield (string + question)
            if answer.lower() in ["n", "no"]:
                caller.msg("|rDeletion cancelled.|n")
                return

            try:
                success = protlib.delete_prototype(self.args)
            except protlib.PermissionError as err:
                retmsg = f"|rError deleting:|R {err}|n"
            else:
                retmsg = (
                    "Deletion successful"
                    if success
                    else "Deletion failed (does the prototype exist?)"
                )
            caller.msg(retmsg)
            return

        if "update" in self.switches:
            # update existing prototypes
            prototype_key = self.args.strip().lower()
            self._update_existing_objects(self.caller, prototype_key)
            return

        # If we get to this point, we use not switches but are trying a
        # direct creation of an object from a given prototype or -key

        prototype = self._parse_prototype(
            self.args, expect=dict if self.args.strip().startswith("{") else str
        )
        if not prototype:
            # this will only let through dicts or strings
            return

        key = "<unnamed>"
        if isinstance(prototype, str):
            # A prototype key we are looking to apply
            prototype_key = prototype
            prototype = self._search_prototype(prototype_key)

            if not prototype:
                return

        # proceed to spawning
        try:
            for obj in spawner.spawn(prototype):
                self.caller.msg("Spawned %s." % obj.get_display_name(self.caller))
                if not prototype.get("location") and not noloc:
                    # we don't hardcode the location in the prototype (unless the user
                    # did so manually) - that would lead to it having to be 'removed' every
                    # time we try to update objects with this prototype in the future.
                    obj.location = caller.location
        except RuntimeError as err:
            caller.msg(err)


class CmdTunnel(COMMAND_DEFAULT_CLASS):
    """
    create new rooms in cardinal directions only

    Usage:
      tunnel[/switch] <direction>[:typeclass] [= <roomname>[;alias;alias;...][:typeclass]]

    Switches:
      oneway - do not create an exit back to the current location
      tel - teleport to the newly created room

    Example:
      tunnel n
      tunnel n = house;mike's place;green building

    This is a simple way to build using pre-defined directions:
     |wn,ne,e,se,s,sw,w,nw|n (north, northeast etc)
     |wu,d|n (up and down)
     |wi,o|n (in and out)
    The full names (north, in, southwest, etc) will always be put as
    main name for the exit, using the abbreviation as an alias (so an
    exit will always be able to be used with both "north" as well as
    "n" for example). Opposite directions will automatically be
    created back from the new room unless the /oneway switch is given.
    For more flexibility and power in creating rooms, use dig.
    """

    key = "tunnel"
    aliases = ["tun"]
    switch_options = ("oneway", "tel")
    locks = "cmd: perm(tunnel) or perm(Builder)"
    help_category = "Building"

    # store the direction, full name and its opposite
    directions = {
        "n": ("norden", "s"),
        "no": ("nordosten", "sw"),
        "o": ("osten", "w"),
        "so": ("südosten", "nw"),
        "s": ("süden", "n"),
        "sw": ("südwesten", "no"),
        "w": ("westen", "o"),
        "nw": ("nordwesten", "so"),
        "ob": ("oben", "u"),
        "u": ("unten", "ob"),
        "re": ("rein", "ra"),
        "ra": ("raus", "re"),
    }

    def func(self):
        """Implements the tunnel command"""

        if not self.args or not self.lhs:
            string = (
                "Usage: tunnel[/switch] <direction>[:typeclass] [= <roomname>"
                "[;alias;alias;...][:typeclass]]"
            )
            self.caller.msg(string)
            return

        # If we get a typeclass, we need to get just the exitname
        exitshort = self.lhs.split(":")[0]

        if exitshort not in self.directions:
            string = "tunnel can only understand the following directions: %s." % ",".join(
                sorted(self.directions.keys())
            )
            string += "\n(use dig for more freedom)"
            self.caller.msg(string)
            return

        # retrieve all input and parse it
        exitname, backshort = self.directions[exitshort]
        backname = self.directions[backshort][0]

        # if we recieved a typeclass for the exit, add it to the alias(short name)
        if ":" in self.lhs:
            # limit to only the first : character
            exit_typeclass = ":" + self.lhs.split(":", 1)[-1]
            # exitshort and backshort are the last part of the exit strings,
            # so we add our typeclass argument after
            exitshort += exit_typeclass
            backshort += exit_typeclass

        roomname = "Some place"
        if self.rhs:
            roomname = self.rhs  # this may include aliases; that's fine.

        telswitch = ""
        if "tel" in self.switches:
            telswitch = "/teleport"
        backstring = ""
        if "oneway" not in self.switches:
            backstring = ", %s;%s" % (backname, backshort)

        # build the string we will use to call dig
        digstring = "dig%s %s = %s;%s%s" % (telswitch, roomname, exitname, exitshort, backstring)
        self.execute_cmd(digstring)


class CmdWhisper(general.CmdWhisper):
    key = "flüster"
    aliases = ["whisper"]
    help_category = "Ingame"

