"""
Commands

Commands describe the input the account can do to the game.

Classes:
    CmdStatus
    CmdTest
    CmdFullHealth

"""

from django.conf import settings
from evennia import default_cmds
from evennia.utils import utils

COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)
CMD_IGNORE_PREFIXES = settings.CMD_IGNORE_PREFIXES


class CmdStatus(COMMAND_DEFAULT_CLASS):
    """
    show character status

    Usage:
      status
    
    Show stats like ability scores, level, experience and hitpoints.
    """
    
    key = "status"
    aliases = ['st']
    locks = "cmd:all()"
    
    def func(self):
        """show status"""
        
        caller = self.caller
        
        padding = 5
        
        string = f'''\
|CName:|n {caller.key}

|CTypus:|n {caller.db.typus}
|CLevel:|n {caller.db.level}
|CExp:|n   {caller.db.exp}

|CHp:|n {caller.db.hp}/{caller.db.hp_max}

|CMU:|n $pad({caller.db.abilities['MU']},2,r)
|CKL:|n $pad({caller.db.abilities['KL']},2,r)
|CCH:|n $pad({caller.db.abilities['CH']},2,r)
|CGE:|n $pad({caller.db.abilities['GE']},2,r)
|CKK:|n $pad({caller.db.abilities['KK']},2,r)

|CAttack:|n $pad({caller.db.attack},2,r)
|CParade:|n $pad({caller.db.parade},2,r)
'''

        caller.msg(string)


class CmdTest(COMMAND_DEFAULT_CLASS):
    """
    a command to test things

    Usage:
      test
    
    A command that only exists to test different things.
    It's nature can change constantly, based on what is
    being tested at that moment.
    """
    
    key = "test"
    locks = "cmd:perm(Developer)"
    
    def func(self):
        """testing"""
        
        caller = self.caller
        
        caller.msg(dir(self))


class CmdFullHealth(default_cmds.ObjManipCommand):
    """
    restore health of character or npc

    Usage:
        fullhealth
        fullhealth <obj>

    This command sets the 'hp' attribute of an object to the
    value of it's 'hp_max' attribute. if no object is specified,
    then the target is the one who is executing the command.
    """
    
    key = "fullhealth"
    aliases = ['fh']
    locks = "cmd:perm(Builder)"
    help_category = "Building"
    
    def func(self):
        caller = self.caller
        args = self.args
        
        if args:
            obj = caller.search(
                args,
                candidates=caller.location.contents + caller.contents,
                use_nicks=True,
            )
            if not obj:
                return
        else:
            obj = caller
        
        obj.db.hp = obj.db.hp_max
        
        caller.msg(f"Hp of {obj} has been restored to {obj.db.hp_max}")
