"""
Overridden Contributed Commands

These are commands from the 'contrib' directory of evennia.
But with changes to language and sometimes functionality.

Classes:
    CmdDetail
    CmdExtendedLook

"""

from django.conf import settings
from evennia import default_cmds
from evennia.commands.default import general
from evennia.utils import utils
from typeclasses.exits import Door

# error return function, needed by Extended Look command
_AT_SEARCH_RESULT = utils.variable_from_module(*settings.SEARCH_AT_RESULT.rsplit(".", 1))

COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)


class CmdDetail(default_cmds.MuxCommand):

    """
    sets a detail on a room

    Usage:
        @detail[/del] <key> [= <description>]
        @detail <key>;<alias>;... = description

    Example:
        @detail
        @detail walls = The walls are covered in ...
        @detail castle;ruin;tower = The distant ruin ...
        @detail/del wall
        @detail/del castle;ruin;tower

    This command allows to show the current room details if you enter it
    without any argument.  Otherwise, sets or deletes a detail on the current
    room, if this room supports details like an extended room. To add new
    detail, just use the @detail command, specifying the key, an equal sign
    and the description.  You can assign the same description to several
    details using the alias syntax (replace key by alias1;alias2;alias3;...).
    To remove one or several details, use the @detail/del switch.

    """

    key = "detail"
    locks = "cmd:perm(Builder)"
    help_category = "Building"

    def func(self):
        location = self.caller.location
        if not self.args:
            details = location.db.details
            if not details:
                self.msg("|rThe room {} doesn't have any detail set.|n".format(location))
            else:
                details = sorted(["|y{}|n: {}".format(key, desc) for key, desc in details.items()])
                self.msg("Details on Room:\n" + "\n".join(details))
            return

        if self.rhs is None and "del" not in self.switches:
            detail = location.return_detail(self.lhs)
            if detail:
                self.msg("Detail '|y{}|n' on Room:\n{}".format(self.lhs, detail))
            else:
                self.msg("Detail '{}' not found.".format(self.lhs))
            return

        method = "set_detail" if "del" not in self.switches and self.rhs != "" else "del_detail"
        #if not hasattr(location, method):
        #    self.caller.msg("Details cannot be set on %s." % location)
        #    return
        for key in self.lhs.split(";"):
            # loop over all aliases, if any (if not, this will just be
            # the one key to loop over)
            getattr(location, method)(key, self.rhs)
        if "del" in self.switches or self.rhs == "":
            self.caller.msg("Detail %s deleted, if it existed." % self.lhs)
        else:
            self.caller.msg("Detail set '%s': '%s'" % (self.lhs, self.rhs))


class CmdExtendedLook(general.CmdLook):
    key = "schau"
    aliases = ["schaue", "betrachte", "b", "look", "l", "ls"]
    help_category = "Ingame"

    def func(self):
        """
        Handle the looking - add fallback to details.
        """
        caller = self.caller
        args = self.args
        if args:
            looking_at_obj = caller.search(
                args,
                candidates=caller.location.contents + caller.contents,
                use_nicks=True,
                quiet=True,
            )
            if not looking_at_obj:
                # no object found. Check if there is a matching
                # detail at location.
                location = caller.location
                if (
                    location
                    and hasattr(location, "return_detail")
                    and callable(location.return_detail)
                ):
                    detail = location.return_detail(args)
                    if detail:
                        # we found a detail instead. Show that.
                        caller.msg(detail)
                        return
                # no detail found. Trigger delayed error messages
                _AT_SEARCH_RESULT(looking_at_obj, caller, args, quiet=False)
                return
            else:
                # we need to extract the match manually.
                if len(utils.make_iter(looking_at_obj)) > 1:
                    # let the system throw an error
                    # if there are multiple matches
                    _AT_SEARCH_RESULT(looking_at_obj, caller, args, quiet=False)
                    return
                else:
                    # continue to handle it manually if there is only one match
                    looking_at_obj = utils.make_iter(looking_at_obj)[0]
        else:
            looking_at_obj = caller.location
            if not looking_at_obj:
                caller.msg("You have no location to look at!")
                return

        if not hasattr(looking_at_obj, "return_appearance"):
            # this is likely due to us having an account instead
            looking_at_obj = looking_at_obj.character
        if not looking_at_obj.access(caller, "view"):
            caller.msg("Could not find '%s'." % args)
            return
        # get object's appearance
        caller.msg(looking_at_obj.return_appearance(caller))
        # the object's at_desc() method.
        looking_at_obj.at_desc(looker=caller)


class CmdOpen(default_cmds.CmdOpen):
    """
    open a new exit from the current room

    Usage:
      @open <new exit>[;alias;alias..][:typeclass] [,<return exit>[;alias;..][:typeclass]]] = <destination>

    Handles the creation of exits. If a destination is given, the exit
    will point there. The <return exit> argument sets up an exit at the
    destination leading back to the current room. Destination name
    can be given both as a #dbref and a name, if that name is globally
    unique.

    """
    # overloading parts of the default CmdOpen command to support doors.
    
    key = "@open"

    def create_exit(self, exit_name, location, destination, exit_aliases=None, typeclass=None):
        """
        Simple wrapper for the default CmdOpen.create_exit
        """
        # create a new exit as normal
        new_exit = super().create_exit(
            exit_name, location, destination, exit_aliases=exit_aliases, typeclass=typeclass
        )
        if hasattr(self, "return_exit_already_created"):
            # we don't create a return exit if it was already created (because
            # we created a door)
            del self.return_exit_already_created
            return new_exit
        if utils.inherits_from(new_exit, Door):
            # a door - create its counterpart and make sure to turn off the default
            # return-exit creation of CmdOpen
            self.caller.msg(
                "Note: A door-type exit was created - ignored eventual custom return-exit type."
            )
            self.return_exit_already_created = True
            back_exit = self.create_exit(
                exit_name, destination, location, exit_aliases=exit_aliases, typeclass=typeclass
            )
            new_exit.db.return_exit = back_exit
            back_exit.db.return_exit = new_exit
        return new_exit


# A simple example of a command making use of the door exit class'
# functionality. One could easily expand it with functionality to
# operate on other types of open-able objects as needed.


class CmdOpenCloseDoor(default_cmds.MuxCommand):
    """
    Öffne und schließe eine Tür

    Usage:
        öffne <Tür>
        schließe <Tür>

    """

    key = "öffne"
    aliases = ["schließe", "schließ", "open", "close"]
    locks = "cmd:all()"
    help_category = "Ingame"

    def func(self):
        "implement the door functionality"
        if not self.args:
            self.caller.msg("Usage: open||close <door>")
            return

        door = self.caller.search(self.args)
        if not door:
            return
        if not utils.inherits_from(door, Door):
            self.caller.msg("This is not a door.")
            return

        if self.cmdstring in ["öffne", "open"]:
            if door.locks.check(self.caller, "traverse"):
                self.caller.msg("%s ist bereits offen." % door.key)
            else:
                door.setlock("traverse:true()")
                self.caller.msg("Du öffnest %s." % door.key)
        else:  # close
            if not door.locks.check(self.caller, "traverse"):
                self.caller.msg("%s ist bereits geschlossen." % door.key)
            else:
                door.setlock("traverse:false()")
                self.caller.msg("Du schließt %s." % door.key)
