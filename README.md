# Ein DSA Multi-User-Dungeon

Ein "Das Schwarze Auge" MUD/MUSH mit Fokus auf Rollenspiel, Quests und komplexen NSC Interaktionen. Als Engine wird [Evennia](https://github.com/evennia/evennia/wiki/Evennia-Introduction/) verwendet.

## Installation

1. [Evennia installieren](https://www.evennia.com/docs/latest/Getting-Started.html) (v0.9.5, Python3.7)
2. - a) Entweder dieses Projekt per `zip` downloaden und in den `muddev` Ordner als `dsamud` entpacken.
   - b) Oder in den muddev Ordner gehen und dann das Projekt mit git klonen: `git clone https://gitlab.com/fire-hawk/dsamud.git`.
3. Mit einem Terminal in den `muddev` Ordner navigieren, wenn nicht schon dort und `source evenv/bin/activate` eingeben. (Dies ändert die Python Entwicklungsumgebung)
4. Jetzt in den `dsamud` Ordner navigieren und dort `evennia migrate` ausführen.
5. Wenn dieser Vorgang abgeschlossen ist `evennia start` ausführen.

Die Webseite müsste nun unter `http://localhost:4001` erreichbar sein.
Man kann sich aber auch per `telnet` oder einem anderen beliebigen MUD-Client verbinden indem man sich ebenfalls zum Server `localhost` verbindet, allerdings hier den Port `4000` verwendet.

_Anmerkung: Ich fand den Ordnernamen `evenv` anfangs ein wenig verwirrend, aufgrund der Tatsache, dass die Engine EVENnia heißt. Tatsächlich setzt sich der Ordnername `evenv` aller Wahrscheinlichkeit nach aus `ev env` zusammen, wobei das `ev` für "evennia" und das `env` für "environment" steht._
