from world.dsa1 import ruleset

#weapon_prototypes

WAFFE_DOLCH = {
	'key': 'Dolch',
	'desc': 'Ein kleiner Dolch.',
	'damage': ruleset.DamageAttrValue(dice_number=1, mod=1), # 1W+1
	'weight': 20,
	'price': 20,
	
	'prototype_desc': 'w+1'
}

WAFFE_KURZSCHWERT = {
	'key': 'Kurzschwert',
	'desc': 'Ein kurzes Schwert.',
	'damage': (1, 2), # 1W+2
	'weight': 60,
	'price': 40,
	
	'prototype_desc': 'w+2'
}

WAFFE_SCHWERT = {
	'key' : 'Schwert',
	'desc': 'Ein gewöhnliches Schwert.',
	'damage' : (1, 4), # 1W+4
	'weight' : 80,
	'price' : 80,
	
	'prototype_desc': 'w+4'
}

WAFFE_ELFENBOGEN = {
	'key': 'Elfenbogen',
	'desc': 'Ein schöner Bogen mit elfischer Handwerkskunst gefertigt',
	'damage': (1, 4), # 1W+4
	'weight': 0,
	'price': 0,
	
	'prototype_desc': 'w+4'
}

#armor_prototypes

ARMOR_KROETENHAUT = {
    'key': 'Krötenhaut',
    'desc': 'Eine grobe Lederweste, die dicht mit Metallnieten bedeckt ist.',
    'armor': 3,
    'hindrance': 2,
    'weight': 0,
    'price': 0,
    
    'prototype_desc': 'armor:3/2'
}

#creature_prototypes

ENEMY_ORC = {
    'key': 'Orc',
    'desc': 'You see an orc.',
    'typeclass': 'typeclasses.characters.NPC',
    'mut': 8,
    'hp_max': 15,
    'hp': 15,
    'armor': 2,
    'attack': 9,
    'parade': 7,
    'damage': (1, 4), # 1w+4
    
    'prototype_desc': 'hp:15'
}

ENEMY_GOBLIN = {
    'key': 'Goblin',
    'desc': 'You see a goblin.',
    'typeclass': 'typeclasses.characters.NPC',
    'mut': 5,
    'hp_max': 12,
    'hp': 12,
    'armor': 2,
    'attack': 7,
    'parade': 6,
    'damage': (1, 2), # 1w+2
    
    'prototype_desc': 'hp:12'
}

ENEMY_OGER = {
    'key': 'Oger',
    'desc': 'You see an oger.',
    'typeclass': 'typeclasses.characters.NPC',
    'mut': 22,
    'hp_max': 40,
    'hp': 40,
    'armor': 3,
    'attack': 6,
    'parade': 5,
    'damage': (2, 4), # 2w+4
    
    'prototype_desc': 'hp:40'
}

ENEMY_TROLL = {
    'key': 'Troll',
    'desc': 'You see a troll.',
    'typeclass': 'typeclasses.characters.NPC',
    'mut': 25,
    'hp_max': 50,
    'hp': 50,
    'armor': 3,
    'attack': 8,
    'parade': 8,
    'damage': (3, 0), # 3w
    
    'prototype_desc': 'hp:50'
}

ENEMY_KOBOLD = {
    'key': 'Kobold',
    'desc': 'You see a kobold.',
    'typeclass': 'typeclasses.characters.NPC',
    'mut': 10,
    'hp_max': 20,
    'hp': 20,
    'armor': 1,
    'attack': 16,
    'parade': 4,
    'damage': (1, 0), # 1w
    
    'prototype_desc': 'hp:20'
}

ENEMY_TATZELWURM = {
    'key': 'Tatzelwurm',
    'desc': 'You see a tatzelwurm.',
    'typeclass': 'typeclasses.characters.NPC',
    'mut': 20,
    'hp_max': 50,
    'hp': 50,
    'armor': 4,
    'attack': 7,
    'parade': 5,
    'attacks': {"claws": (1, 0), "bite": (2, 0), "tail": (1, 0)}, # 1w, 2w, 1w
    'attacks_per_round': 2,
    
    'prototype_desc': 'hp:50'
}

ENEMY_SOLDIER = {
    'key': 'Soldier',
    'desc': 'A human serving the militia or military.',
    'typeclass': 'typeclasses.characters.NPC',
    'mut': 10,
    'hp_max': 20,
    'hp': 20,
    'armor': 3,
    'attack': 10,
    'parade': 8,
    'damage': (1, 4), # 1w+4
    
    'prototype_desc': 'hp:20'
}
