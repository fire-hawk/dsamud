from django.conf import settings
from evennia.utils import utils
from evennia import EvMenu
from random import SystemRandom
random = SystemRandom()

COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)


coins_factor = {'dukaten': 10, 'silbertaler': 1, 'heller': .1, 'kreuzer': .01}


def roll_attributes():
    attr = {"MU":None,"KL":None,"CH":None,"GE":None,"KK":None}
    for key in attr:
        attr[key] = 7 + random.randint(1, 6)
    return attr


def available_classes(attr):
    classes = {"Adventurer"}
    if attr["MU"] >= 12 and attr["KK"] >= 12:
        classes.add("Warrior")
    if attr["KK"] >= 12 and attr["GE"] >= 12:
        classes.add("Dwarf")
    if attr["KL"] >= 12 and attr["GE"] >= 12:
        classes.add("Elv")
    if attr["KL"] >= 12 and attr["CH"] >= 12:
        classes.add("Magician")
    return classes


def starting_stats(attr, typus):
    stats = {}
    stats["level"] = 1
    stats["exp"] = 0
    stats["attack"] = 10
    stats["parade"] = 8
    if typus == "Adventurer":
        stats["hp"] = 30
    elif typus == "Warrior":
        stats["hp"] = 30
    elif typus == "Dwarf":
        stats["hp"] = 35
    elif typus == "Elv":
        stats["hp"] = 25
        stats["ap"] = 25
    elif typus == "Magician":
        stats["hp"] = 20
        stats["ap"] = 30
    return stats


def in_silbertaler(coins_dict):
    silbertaler = 0
    for key in coins_dict:
        silbertaler += coins_dict[key] * coins_factor[key]
    return silbertaler


def in_coins(silbertaler):
    kreuzer = silbertaler * 100
    string = str(kreuzer)
    coins_dict = {}
    
    coins_dict["dukaten"] = string[0]
    coins_dict["silbertaler"] = string[1]
    coins_dict["heller"] = string[2]
    coins_dict["kreuzer"] = string[3]
    
    return coins_dict


class CmdAttack(COMMAND_DEFAULT_CLASS):
    """
    attack an other player or npc

    Usage:
      attack <obj>

    Attacks an other character with bare hands or a weapon.
    """

    key = "attack"
    aliases = ['att']
    help_category = "Ingame"
    locks = "cmd:all()"

    def func(self):
        """
        Handle the attack.
        """
        
        attacker = self.caller
        location = self.caller.location
        
        # low hitpoints
        if attacker.db.hp <= 5:
            attacker.msg("You can't attack with that amount of hitpoints.")
            return
        
        # attack air
        if not self.args:
            attacker.msg("You attack the air")
            attacker.location.msg_contents('{} attacks the air'.format(self.caller.key),exclude=[self.caller])
            return
        
        # find target
        defender = self.caller.search(self.args)
        if defender is None:
            return
        
        # find relevant stats
        if attacker.db.damage is None:
            if attacker.db.weapon is None:
                attacker_damage = (1, 0)
            else:
                attacker_damage = attacker.db.weapon.db.damage
        else:
            attacker_damage = attacker.db.damage
        
        if type(defender.db.armor) is int:
            defender_armor = defender.db.armor
        elif defender.db.armor is None:
            defender_armor = 0
        else:
            defender_armor = defender.db.armor.db.armor
        
        # hitpoints color
        hp_low = (defender.db.hp_max - 5) / 3
        hp_mid = (defender.db.hp_max - 5) / 3 * 2
        if defender.db.hp <= (hp_low + 5):
            hp_color = "|R"
        elif defender.db.hp <= (hp_mid + 5):
            hp_color = "|Y"
        else:
            hp_color = "|G"
         
        
        # do the actual attack
        rolls = []
        rolls.append(random.randint(1, 20)) 
        if rolls[0] <= attacker.db.attack:
            rolls.append(random.randint(1, 20))
            if rolls[1] <= defender.db.parade:
                location.msg_contents(f"{attacker.name} attacks |x({rolls[0]})|n {defender.name}|x({hp_color}{defender.db.hp}|x)|n, but {defender.name} defends |x({rolls[1]})|n.")
            else:
                for i in range(attacker_damage[0]):
                    rolls.append(random.randint(1, 6))
                rolls_string = "+".join(str(x) for x in rolls[2:])
                if attacker_damage[1] is not 0:
                    rolls_string += "+" + str(attacker_damage[1])
                total_damage = 0
                for roll in rolls[2:]:
                    total_damage += roll
                total_damage += attacker_damage[1]
                effective_damage = total_damage - defender_armor
                if effective_damage < 0:
                    effective_damage = 0
                location.msg_contents(f"{attacker.name} attacks |x({rolls[0]})|n {defender.name}|x({hp_color}{defender.db.hp}|x)|n, he gets hit |x({rolls[1]})|n and endures |r{effective_damage}|n damage of {total_damage}|x({rolls_string})|n.")
                defender.db.hp -= effective_damage
                
                if defender.db.hp <= 5:
                    location.msg_contents(f"|r{defender.name}|R becomes defeated.|n")
                    if defender.typename is "NPC":
                        defender.delete()
                        return
                
        else:
            location.msg_contents(f"{attacker.name} attacks |x({rolls[0]})|n {defender.name}|x({hp_color}{defender.db.hp}|x)|n, but misses.")
        
        defender.at_after_attack(attacker)


class DamageAttrValue(object):
    """
    A nice representation of the damage
    attribute.
    the __str__ method will make it look
    like these examples:
    
    'w+1'
    'w+4'
    '2w+4'
    
    instead of having a tuple like:
    
    (1, 1)
    (1, 4)
    (2, 4)
    """
    def __init__(self, dice_number=None, mod=None):
        self.dice_number = dice_number
        self.mod = mod
    
    def __str__(self):
        if self.dice_number == 0:
            return str(self.mod)
        
        string = ""
        if self.dice_number > 1:
            string += str(self.dice_number)
        string += "w"
        if self.mod > 0:
            string += "+" + str(self.mod)
        
        return string
    
    def __repr__(self):
        return f"({self.dice_number}, {self.mod})"
    
    def __getitem__(self, key):
        if key == 0:
            return self.dice_number
        elif key == 1:
            return self.mod
        else:
            raise IndexError("index out of range")

#hostile_character_ai

