from random import SystemRandom
random = SystemRandom()
from django.conf import settings
from evennia.utils import create, logger

_MAX_NR_CHARACTERS = settings.MAX_NR_CHARACTERS


def start(caller, raw_string, **kwargs):
    stats = {"MU":None,"KL":None,"CH":None,"GE":None,"KK":None}
    for key in stats:
        stats[key] = 7 + random.randint(1, 6)
    
    text = ""
    for key in stats:
        text += "{}: {}\n".format(key, stats[key])
    
    caller.ndb.chargen_data = {}
    caller.ndb.chargen_data["stats_dict"] = stats

    options = ({"desc": "Adventurer", "goto": ("node_2", {"archetype": "Adventurer"})},)
    if stats["MU"] >= 12 and stats["KK"] >= 12:
        options = options + ({"desc": "Warrior", "goto": ("node_2", {"archetype": "Warrior"})},)
    if stats["KK"] >= 12 and stats["GE"] >= 12:
        options = options + ({"desc": "Dwarf", "goto": ("node_2", {"archetype": "Dwarf"})},)
    if stats["KL"] >= 12 and stats["GE"] >= 12:
        options = options + ({"desc": "Elv", "goto": ("node_2", {"archetype": "Elv"})},)
    if stats["KL"] >= 12 and stats["CH"] >= 12:
        options = options + ({"desc": "Magician", "goto": ("node_2", {"archetype": "Magician"})},)

    text += "\nAre you statisfied with these stats, then choose a class."
    text += "\nOr roll again."

    options = options + ({"key": "r", "desc": "roll again", "goto": "start"},)

    return text, options


def node_2(caller, raw_string, **kwargs):

    # start node feedback #
    caller.ndb.chargen_data["archetype"] = kwargs["archetype"]
    caller.msg("\nYou chose: " + kwargs["archetype"])
    # start node feedback #
    
    text = "What gender?"
    
    options = ({"key": "m", "desc": "male", "goto": ("node_3", {"gender": "male"})},
               {"key": "f", "desc": "female", "goto": ("node_3", {"gender": "female"})})

    return text, options


def node_3(caller, raw_string, **kwargs):

    # node_2 feedback #
    caller.ndb.chargen_data["gender"] = kwargs["gender"]
    caller.msg("\nYou chose: " + kwargs["gender"])
    # node_2 feedback #
    
    text = "Enter your character's name."

    options = {"key": "_default",
               "goto": "node_4"}

    return text, options


def node_4(caller, raw_string, **kwargs):

    # node_3 processing #
    caller.ndb.chargen_data["key"] = raw_string.strip()
    caller.msg("\nYour character's name is: " + caller.ndb.chargen_data["key"])
    # node_3 processing #
    
    text = "What does an other player see\n"
    text += "when he looks at your character:"

    options = {"key": "_default",
               "goto": "node_5"}

    return text, options


def node_5(caller, raw_string, **kwargs):

    # node_4 processing #
    caller.ndb.chargen_data["desc"] = raw_string.strip()
    caller.msg("\nWhen other players look at your character they will read:")
    caller.msg(caller.ndb.chargen_data["desc"] + "|/|/")
    # node_4 processing #
    
    if caller.db_typeclass_path == "typeclasses.accounts.Account":
        # caller is the account
        account = caller
    else:
        # caller has an account
        account = caller.account

    key = caller.ndb.chargen_data["key"]
    desc = caller.ndb.chargen_data["desc"]

    charmax = _MAX_NR_CHARACTERS

    if not account.is_superuser and (
        account.db._playable_characters and len(account.db._playable_characters) >= charmax
    ):
        plural = "" if charmax == 1 else "s"
        caller.msg(f"You may only create a maximum of {charmax} character{plural}.")
        return
    from evennia.objects.models import ObjectDB

    typeclass = settings.BASE_CHARACTER_TYPECLASS

    if ObjectDB.objects.filter(db_typeclass_path=typeclass, db_key__iexact=key):
        # check if this Character already exists. Note that we are only
        # searching the base character typeclass here, not any child
        # classes.
        caller.msg("|rA character named '|w%s|r' already exists.|n" % key)
        return

    # create the character
    start_location = ObjectDB.objects.get_id(settings.START_LOCATION)
    default_home = ObjectDB.objects.get_id(settings.DEFAULT_HOME)
    permissions = settings.PERMISSION_ACCOUNT_DEFAULT
    new_character = create.create_object(
        typeclass, key=key, location=start_location, home=default_home, permissions=permissions
    )
    # only allow creator (and developers) to puppet this char
    new_character.locks.add(
        "puppet:id(%i) or pid(%i) or perm(Developer) or pperm(Developer);delete:id(%i) or perm(Admin)"
        % (new_character.id, account.id, account.id)
    )
    account.db._playable_characters.append(new_character)
    if desc:
        new_character.db.desc = desc
    elif not new_character.db.desc:
        new_character.db.desc = "This is a character."
    
    typus = caller.ndb.chargen_data["archetype"]
    
    new_character.db.abilities = caller.ndb.chargen_data["stats_dict"]
    new_character.db.typus = caller.ndb.chargen_data["archetype"]
    new_character.db.gender = caller.ndb.chargen_data["gender"]
    if typus == "Warrior":
        new_character.db.hp = 30
        new_character.db.hp_max = 30
    elif typus == "Dwarf":
        new_character.db.hp = 35
        new_character.db.hp_max = 35
    elif typus == "Elv":
        new_character.db.hp = 25
        new_character.db.hp_max = 25
        new_character.db.ap = 25
        new_character.db.ap_max = 25
    elif typus == "Magician":
        new_character.db.hp = 20
        new_character.db.hp_max = 20
        new_character.db.ap = 30
        new_character.db.ap_max = 30
    
    new_character.db.level = 1
    new_character.db.exp = 0
    new_character.db.attack = 10
    new_character.db.parade = 8
    new_character.db.coins = {"dukaten": 0, "silbertaler": 0, "heller": 0, "kreuzer": 0}
    
    caller.msg(
        "Created new character %s. Use |wic %s|n to enter the game as this character."
        % (new_character.key, new_character.key)
    )
    logger.log_sec(
        "Character Created: %s (Caller: %s, IP: %s)."
        % (new_character, account, caller.ndb._evmenu._session.address)
    )
