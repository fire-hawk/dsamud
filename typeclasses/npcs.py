"""
NPCs

Characters in the game world, which are not controlled by players,
but have a similar command set.

Classes:
    NPC

"""
import random
from typeclasses.objects import Object
from typeclasses.characters import Character


class NPC(Character, Object):
    """
    The Character defaults to reimplementing some of base Object's hook methods with the
    following functionality:

    at_basetype_setup - always assigns the DefaultCmdSet to this object type
                    (important!)sets locks so character cannot be picked up
                    and its commands only be called by itself, not anyone else.
                    (to change things, use at_object_creation() instead).
    at_after_move(source_location) - Launches the "look" command after every move.
    at_post_unpuppet(account) -  when Account disconnects from the Character, we
                    store the current location in the pre_logout_location Attribute and
                    move it to a None-location so the "unpuppeted" character
                    object does not need to stay on grid. Echoes "Account has disconnected"
                    to the room.
    at_pre_puppet - Just before Account re-connects, retrieves the character's
                    pre_logout_location Attribute and move it back on the grid.
    at_post_puppet - Echoes "AccountName has entered the game" to the room.

    """

    def at_object_creation(self):
        self.db.hp_max = 20
        self.db.hp = 20
        self.db.attack = 8
        self.db.parade = 6

    def at_after_attack(self, attacker, **kwargs):
        """After being attacked."""
        self.execute_cmd("attack %s" % attacker, None)
        
    def at_tick(self):
        """NPC AI"""
        mumblings = self.db.mumblings
        if random.randint(1, 10) < 5:
            if mumblings is not None:
                self.execute_cmd(random.choice(mumblings))

