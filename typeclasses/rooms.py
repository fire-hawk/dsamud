"""
Room

Rooms are simple containers that has no location of their own.

Classes:
    Room

"""

from evennia import DefaultRoom
from typeclasses.objects import Object


class Room(Object, DefaultRoom):
    """
    Rooms are like any Object, except their location is None
    (which is default). They also use basetype_setup() to
    add locks so they cannot be puppeted or picked up.
    (to change that, use at_object_creation instead)

    See examples/object.py for a list of
    properties and methods available on all Objects.
    """

    def set_detail(self, detailkey, description):
        """
        This sets a new detail, using an Attribute "details".

        Args:
            detailkey (str): The detail identifier to add (for
                aliases you need to add multiple keys to the
                same description). Case-insensitive.
            description (str): The text to return when looking
                at the given detailkey.

        """
        if self.db.details:
            self.db.details[detailkey.lower()] = description
        else:
            self.db.details = {detailkey.lower(): description}
    
    def return_detail(self, key):
        """
        This will attempt to match a "detail" to look for in the room.

        Args:
            key (str): A detail identifier.

        Returns:
            detail (str or None): A detail matching the given key.

        Notes:
            A detail is a way to offer more things to look at in a room
            without having to add new objects. For this to work, we
            require a custom `look` command that allows for `look
            <detail>` - the look command should defer to this method on
            the current location (if it exists) before giving up on
            finding the target.

            Details are not season-sensitive, but are parsed for timeslot
            markers.
        """
        try:
            detail = self.db.details.get(key.lower(), None)
        except AttributeError:
            # this happens if no attribute details is set at all
            return None
        if detail:
            # season, timeslot = self.get_time_and_season()
            # detail = self.replace_timeslots(detail, timeslot)
            return detail
        return None

    def del_detail(self, detailkey, description):
        """
        Delete a detail.

        The description is ignored.

        Args:
            detailkey (str): the detail to remove (case-insensitive).
            description (str, ignored): the description.

        The description is only included for compliance but is completely
        ignored.  Note that this method doesn't raise any exception if
        the detail doesn't exist in this room.

        """
        if self.db.details and detailkey.lower() in self.db.details:
            del self.db.details[detailkey.lower()]
